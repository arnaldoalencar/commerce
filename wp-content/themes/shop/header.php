<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Look After</title>

        <!-- CSS -->
        <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <?php wp_head(); ?>
    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <?php

	                    if ( is_front_page() && is_home() ) : 
	                ?>
	               <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
                    <?php else : ?>
                    	 <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>	
                    <?php endif;
	                	$description = get_bloginfo( 'description', 'display' );
	                    if ( $description || is_customize_preview() ) : ?>
	                    	  <?php echo $description; ?></a>
	                <?php endif; 
	                ?>
                   <!--  <a class="navbar-brand" href="#">Project name</a> -->
                </div>
                 <div id="navbar" class="navbar-collapse collapse">
                    <div class="navbar-form navbar-right">
					    <?php
							$args = array(
							    'menu' => 'Main Menu',
							    'sort_column' => 'menu_order',
							    'container' => 'div',
							);
						wp_nav_menu( $args ); 
						?>
                    </div>
                </div><!--/.navbar-collapse -->
                
            </div>
        </nav>

         <!-- Page Header -->
    <?php if (is_front_page()): ?>     
    <header class="masthead" style="background-image: url('wp-content/themes/shop/img/banner_cap.jpg')">
      <div class="overlay"></div>
      <div class="container">
        <div class="row">
        
        </div>
      </div>
    </header>
    <?php endif; ?>