<?php get_header(); ?>
        <div class="container">
            <!-- Example row of columns -->
            <div class="row">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                     <div class="col-md-12">
                        <?php the_content(); ?>
                    </div>

                <?php endwhile; else: ?>
                <p><?php _e('Desculpe, não há posts a exibir.'); ?></p>
            <?php endif; ?>
            </div>
<?php get_footer(); ?>



