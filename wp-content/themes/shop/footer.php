<hr>
<!-- Footer -->
<footer class="page-footer font-small teal pt-4 footer ">

	<!-- Footer Text -->
	<div class="container-fluid text-md-left">

		<!-- Grid row -->
		<div class="row">

			<!-- Grid column -->
			<div class="col-md-6 mt-md-0 mt-3 ">

				<!-- Content -->
				<h5>ABOUT US</h5>
				<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Expedita sapiente sint, nulla, nihil repudiandae commodi voluptatibus corrupti animi sequi aliquid magnam debitis, maxime quam recusandae harum esse fugiat. Itaque, culpa?</p>

			</div>
			<!-- Grid column -->

			<!-- Grid column -->
			<div class="col-md-6 mb-md-0 mb-3">

				<!-- Content -->
				<h5 class="text-uppercase font-weight-bold">Social Networks</h5>
				<a href="#" class="fa fa-facebook"></a>
				<a href="#" class="fa fa-twitter"></a>
				<a href="#" class="fa fa-instagram"></a>

			</div>
			<!-- Grid column -->

		</div>
		<!-- Grid row -->

	</div>
	<!-- Footer Text -->


</footer>
<!-- Footer -->
<?php wp_footer(); ?>
</body>
</html>